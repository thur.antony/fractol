/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   math.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/22 12:28:03 by avykhova          #+#    #+#             */
/*   Updated: 2018/07/22 12:28:05 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

t_point			map(double x, double y, t_screen *s, t_map *m)
{
	t_point		new;

	new.x = m->x_min + (m->x_max - m->x_min) * (x / s->x_max);
	new.y = m->y_min + (m->y_max - m->y_min) * (y / s->y_max);
	return (new);
}

double			interpolate(double start, double end, double interpolation)
{
	return (start + ((end - start) * interpolation));
}

void			coef_update(t_data *dt)
{
	t_point		start;
	t_point		end;

	start = map(0, 0, &dt->original, &dt->comp_plane);
	end = map(1, 0, &dt->original, &dt->comp_plane);
	dt->c.oef_x = (end.x - start.x) * PIXEL;
	end = map(0, 1, &dt->original, &dt->comp_plane);
	dt->c.oef_y = (end.y - start.y) * PIXEL;
}
