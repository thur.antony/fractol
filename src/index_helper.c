/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   index_helper.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/22 12:27:47 by avykhova          #+#    #+#             */
/*   Updated: 2018/07/22 12:27:49 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void			limits_set(t_point *p0, t_point *p1, t_data *dt)
{
	p0->x = dt->comp_plane.x_min;
	p0->y = dt->comp_plane.y_min;
	p1->x = dt->comp_plane.x_min;
	p1->y = dt->comp_plane.y_min;
}

void			screen_init(t_pixel *dot)
{
	dot->x = 0;
	dot->y = 0;
}

void			calc_zero_init(t_point *p, t_nums *n, int *i)
{
	n->zr = 0;
	n->zi = 0;
	n->zr2 = 0;
	n->zi2 = 0;
	n->cr = p->x;
	n->ci = p->y;
	*i = 0;
}

void			calc_two_init(t_point *p, t_nums *n, double mx, double my)
{
	n->zr = p->x;
	n->zi = p->y;
	n->zr2 = n->zr * n->zr;
	n->zi2 = n->zi * n->zi;
	n->cr = mx;
	n->ci = my;
}
