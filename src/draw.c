/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/22 12:27:04 by avykhova          #+#    #+#             */
/*   Updated: 2018/07/22 12:27:06 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void			point(t_data *dt, int x, int y, int color)
{
	int			byte;

	if (x > WIN_WIDTH || x < 0 || y > WIN_HEIGHT || y < 0)
		return ;
	byte = (x * dt->bpp) + (y * dt->size_line);
	dt->image[byte] = color;
	dt->image[++byte] = color >> 8;
	dt->image[++byte] = color >> 16;
}

void			rect(t_data *dt, int x, int y, int color)
{
	int			x_lim;
	int			y_lim;
	int			x_0;

	x_lim = x + PIXEL;
	y_lim = y + PIXEL;
	x_0 = x;
	while (y < y_lim)
	{
		while (x < x_lim)
		{
			point(dt, x, y, color);
			x++;
		}
		x = x_0;
		y++;
	}
}

int				color(int i, double z, double c)
{
	int			r;
	int			g;
	int			b;
	int			color;

	(void)c;
	r = (unsigned char)(7 * i);
	g = (unsigned char)(5 * z);
	b = (unsigned char)(i % 5);
	color = 0;
	color += r;
	color <<= 8;
	color += g;
	color <<= 8;
	color += b;
	return (color);
}
