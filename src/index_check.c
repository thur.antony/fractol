/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   index_check.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/22 12:27:14 by avykhova          #+#    #+#             */
/*   Updated: 2018/07/22 12:27:16 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		index_check(double mousex, double mousey, t_data *dt)
{
	if (dt->index == 0)
		index_zero(dt);
	else if (dt->index == 2)
		index_two(mousex, mousey, dt);
	else if (dt->index == 1)
		index_one(dt);
	else if (dt->index == 3)
		index_three(mousex, mousey, dt);
}
