/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parallel_init.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/27 13:50:40 by avykhova          #+#    #+#             */
/*   Updated: 2018/07/27 13:50:42 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void				parallel_init(t_data *dt)
{
	int				i;
	int				y;
	int				ycoef;

	i = 0;
	y = 0;
	ycoef = WIN_HEIGHT / 8;
	while (i < 8)
	{
		dt->p[i].dot = map(0, y, &dt->original, &dt->comp_plane);
		dt->p[i].x0 = dt->p[0].dot.x;
		dt->p[i].s.x = 0;
		dt->p[i].s.y = y;
		dt->p[i].end.x = WIN_WIDTH;
		y += ycoef;
		dt->p[i].end.y = y;
		i++;
	}
}
