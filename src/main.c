/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/22 12:27:56 by avykhova          #+#    #+#             */
/*   Updated: 2018/07/22 12:27:58 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static int		exit_x(void)
{
	exit(1);
	return (0);
}

int				main(int argc, char **argv)
{
	t_data		dt;
	int			index;

	R((index = test_argc(argc, argv)));
	dt = init(argv[1], index);
	mlx_mouse_hook(dt.win_ptr, zoom, &dt);
	mlx_key_hook(dt.win_ptr, controls, &dt);
	mlx_hook(dt.win_ptr, 17, 1L << 17, exit_x, NULL);
	mlx_hook(dt.win_ptr, 6, 1L << 6, mouse_check, &dt);
	mlx_loop(dt.mlx_ptr);
	return (0);
}
