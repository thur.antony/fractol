/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/22 12:27:38 by avykhova          #+#    #+#             */
/*   Updated: 2018/07/22 12:27:39 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int				zoom(int button, int x, int y, t_data *dt)
{
	double		zoom_factor;
	t_point		mouse;

	if (button == 5)
		zoom_factor = 0.9;
	else if (button == 4)
		zoom_factor = 1.1;
	else
		return (0);
	mouse = map(x, y, &dt->original, &dt->comp_plane);
	dt->comp_plane.x_min = interpolate(mouse.x, dt->comp_plane.x_min, \
		zoom_factor);
	dt->comp_plane.x_max = interpolate(mouse.x, dt->comp_plane.x_max, \
		zoom_factor);
	dt->comp_plane.y_min = interpolate(mouse.y, dt->comp_plane.y_min, \
		zoom_factor);
	dt->comp_plane.y_max = interpolate(mouse.y, dt->comp_plane.y_max, \
		zoom_factor);
	coef_update(dt);
	parallel_init(dt);
	index_check(mouse.x, mouse.y, dt);
	mlx_put_image_to_window(dt->mlx_ptr, dt->win_ptr, dt->img_ptr, 0, 0);
	return (1);
}

int				mouse_check(int x, int y, t_data *dt)
{
	t_point		mouse;

	if (!dt->mouse_state || dt->index < 2)
		return (1);
	mouse = map(x, y, &dt->original, &dt->comp_plane);
	coef_update(dt);
	parallel_init(dt);
	index_check(mouse.x, mouse.y, dt);
	mlx_put_image_to_window(dt->mlx_ptr, dt->win_ptr, dt->img_ptr, 0, 0);
	return (1);
}

int				controls(int button, t_data *dt)
{
	if (button == 49)
		dt->mouse_state = !dt->mouse_state;
	else if (button == 24 || button == 27)
	{
		(button == 24) ? (dt->max_i += 25) : (dt->max_i -= 25);
		if (dt->index < 2)
		{
			coef_update(dt);
			parallel_init(dt);
			index_check(0, 0, dt);
			mlx_put_image_to_window(dt->mlx_ptr, dt->win_ptr, dt->img_ptr, \
			0, 0);
		}
	}
	else if (button == 53)
	{
		exit(1);
		return (0);
	}
	return (1);
}
