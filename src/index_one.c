/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   index_one.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/27 13:44:39 by avykhova          #+#    #+#             */
/*   Updated: 2018/07/27 13:44:41 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void				index_one(t_data *dt)
{
	pthread_t		pt[8];
	int				i;
	t_pthread_arg	arg[8];

	i = 0;
	while (i < 8)
	{
		arg[i].dt = dt;
		arg[i].i = i;
		pthread_create(&pt[i], NULL, index_one_ploop, (void *)&(arg[i]));
		i++;
	}
	i = 7;
	while (i >= 0)
	{
		pthread_join(pt[i], NULL);
		i--;
	}
}

void				*index_one_ploop(void *arg)
{
	t_pthread_arg	*a;
	t_data			*dt;
	int				i;

	a = (t_pthread_arg	*)arg;
	dt = a->dt;
	i = a->i;
	while (dt->p[i].s.y < dt->p[i].end.y)
	{
		while (dt->p[i].s.x < dt->p[i].end.x)
		{
			rect(dt, dt->p[i].s.x, dt->p[i].s.y, calc_one(dt->p[i].dot, \
				dt->max_i));
			dt->p[i].s.x += PIXEL;
			dt->p[i].dot.x += dt->c.oef_x;
		}
		dt->p[i].s.x = 0;
		dt->p[i].dot.x = dt->p[i].x0;
		dt->p[i].s.y += PIXEL;
		dt->p[i].dot.y += dt->c.oef_y;
	}
	return (NULL);
}

int					calc_one(t_point p, int iter)
{
	t_nums			n;
	int				i;

	calc_zero_init(&p, &n, &i);
	while (n.zr2 + n.zi2 < 4 && i < iter)
	{
		n.zi = pow((n.zr + n.zi), 2) - n.zr2 - n.zi2;
		n.zi = fabs(n.zi) + n.ci;
		n.zr = fabs(n.zr2 - n.zi2 + n.cr);
		n.zr2 = pow(n.zr, 2);
		n.zi2 = pow(n.zi, 2);
		i++;
	}
	if (i < iter)
		return (color(i, n.zi2, n.zr2));
	return (WHITE);
}
