/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_helper.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/22 12:27:31 by avykhova          #+#    #+#             */
/*   Updated: 2018/07/22 12:27:33 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int				usage(void)
{
	int			i;

	i = -1;
	ft_putendl("\nUSAGE: ./fractol + arguments");
	ft_putendl("list of available args/fractals:");
	ft_putendl("[m]  - mandelbrot");
	ft_putendl("[j]  - julia");
	ft_putendl("[bs] - burning ship");
	ft_putendl("[bj] - burning ship -> julia");
	ft_putchar('\n');
	return (-1);
}

int				test_argc(int argc, char **argv)
{
	int			finded;
	int			i;
	int			j;

	finded = 0;
	i = 0;
	j = -1;
	if (argc != 2)
		return (usage());
	else if (!(ft_strcmp(argv[1], "m")))
		return (0);
	else if (!(ft_strcmp(argv[1], "bs")))
		return (1);
	else if (!(ft_strcmp(argv[1], "j")))
		return (2);
	else if (!(ft_strcmp(argv[1], "jb")))
		return (3);
	return (usage());
}

t_data			init(char *name, int index)
{
	t_data		dt;

	dt.name = name;
	dt.index = index;
	dt.mlx_ptr = mlx_init();
	dt.win_ptr = mlx_new_window(dt.mlx_ptr, WIN_WIDTH, WIN_HEIGHT, name);
	dt.img_ptr = mlx_new_image(dt.mlx_ptr, WIN_WIDTH, WIN_HEIGHT + 1);
	dt.image = mlx_get_data_addr(dt.img_ptr, &dt.bpp, &dt.size_line, &dt.end);
	dt.bpp /= 8;
	dt.comp_plane.x_min = -3;
	dt.comp_plane.x_max = 3;
	dt.comp_plane.y_min = -3;
	dt.comp_plane.y_max = 3;
	dt.original.x_min = 0;
	dt.original.x_max = WIN_WIDTH;
	dt.original.y_min = 0;
	dt.original.y_max = WIN_HEIGHT;
	dt.mouse_state = 1;
	dt.max_i = MAX_ITER;
	parallel_init(&dt);
	return (dt);
}
