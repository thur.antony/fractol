/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   index_two.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/22 17:21:32 by avykhova          #+#    #+#             */
/*   Updated: 2018/07/22 17:21:34 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void				index_two(double mousex, double mousey, t_data *dt)
{
	pthread_t		pt[8];
	int				i;
	t_pthread_arg	arg[8];

	i = 0;
	while (i < 8)
	{
		arg[i].dt = dt;
		arg[i].i = i;
		arg[i].mouse.x = mousex;
		arg[i].mouse.y = mousey;
		pthread_create(&pt[i], NULL, index_two_ploop, (void *)&(arg[i]));
		i++;
	}
	i = 7;
	while (i >= 0)
	{
		pthread_join(pt[i], NULL);
		i--;
	}
}

void				*index_two_ploop(void *arg)
{
	t_pthread_arg	*a;
	t_data			*dt;
	t_point			mouse;
	int				i;

	a = (t_pthread_arg	*)arg;
	dt = a->dt;
	i = a->i;
	mouse.x = a->mouse.x;
	mouse.y = a->mouse.y;
	while (dt->p[i].s.y < dt->p[i].end.y)
	{
		while (dt->p[i].s.x < dt->p[i].end.x)
		{
			rect(dt, dt->p[i].s.x, dt->p[i].s.y, calc_two(mouse.x, mouse.y, \
				dt->p[i].dot, dt->max_i));
			dt->p[i].s.x += PIXEL;
			dt->p[i].dot.x += dt->c.oef_x;
		}
		dt->p[i].s.x = 0;
		dt->p[i].dot.x = dt->p[i].x0;
		dt->p[i].s.y += PIXEL;
		dt->p[i].dot.y += dt->c.oef_y;
	}
	return (NULL);
}

int					calc_two(double mousex, double mousey, t_point p, int max_i)
{
	t_nums			n;
	int				i;

	i = 0;
	calc_two_init(&p, &n, mousex, mousey);
	while (n.zr2 + n.zi2 < 4 && i < max_i)
	{
		n.zi = pow((n.zr + n.zi), 2) - n.zr2 - n.zi2;
		n.zi += n.ci;
		n.zr = n.zr2 - n.zi2 + n.cr;
		n.zr2 = pow(n.zr, 2);
		n.zi2 = pow(n.zi, 2);
		i++;
	}
	if (i < max_i)
		return (color(i, n.zi2, n.zr2));
	else
		return (color(i, n.zr2, n.zi2));
}
