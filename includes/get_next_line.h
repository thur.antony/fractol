/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/10 17:03:24 by avykhova          #+#    #+#             */
/*   Updated: 2018/02/10 17:07:01 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include "get_next_line.h"
# include "../libft/libft.h"

# define BUFF_SIZE 100
# define E(x) if (!x) return (-1);
# define F(x) if (x) free (x);

int			get_next_line(const int fd, char **line);
t_list		*right_file(t_list **fd_lst, int fd);
int			read_buffer(t_list *current);
int			extract_line(t_list *current, char **line, int i);
int			last_line(t_list *current, char **line);

#endif
