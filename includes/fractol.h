/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/22 12:26:44 by avykhova          #+#    #+#             */
/*   Updated: 2018/07/22 12:26:47 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include "fractol.h"
# include "get_next_line.h"
# include "mlx.h"
# include <math.h>
# include <fcntl.h>
# include <pthread.h>

# define WIN_WIDTH 700
# define WIN_HEIGHT WIN_WIDTH
# define PIXEL 1
# define MAX_ITER 100
# define IMG_LEN WIN_WIDTH * WIN_HEIGHT  * 4
# define WHITE 16777215
# define R(x) if (x == -1) return (1);
# define STR(s) #s
# define XSTR(s) STR(s)

typedef struct		s_point
{
	double			x;
	double			y;
}					t_point;

typedef struct		s_screen
{
	double			x_min;
	double			x_max;
	double			y_min;
	double			y_max;
}					t_screen;

typedef struct		s_map
{
	double			x_min;
	double			x_max;
	double			y_min;
	double			y_max;
}					t_map;

typedef struct		s_coef
{
	double			oef_x;
	double			oef_y;
}					t_coef;

typedef struct		s_pixel
{
	int				x;
	int				y;
}					t_pixel;

typedef struct		s_parallel
{
	t_point			dot;
	double			x0;
	t_pixel			s;
	t_pixel			end;
}					t_parallel;

typedef struct		s_nums
{
	double			zr;
	double			zi;
	double			cr;
	double			ci;

	double			zr2;
	double			zi2;
}					t_nums;

typedef struct		s_data
{
	char			*name;
	int				index;

	void			*mlx_ptr;
	void			*win_ptr;
	void			*img_ptr;
	char			*image;
	int				bpp;
	int				size_line;
	int				end;
	t_map			comp_plane;
	t_screen		original;
	t_coef			c;

	t_parallel		p[8];

	int				mouse_state;
	int				max_i;
}					t_data;

typedef	struct		s_thread_arg
{
	t_data			*dt;
	t_point			mouse;
	int				i;
}					t_pthread_arg;
/*
**	MAIN_HELPERS.C - INIT
**	-----------------------------------------------------------------
*/
int					usage(void);
int					test_argc(int argc, char **argv);
t_data				init(char *argv, int index);
/*
**	DRAW.C
**	-----------------------------------------------------------------
*/
void				rect(t_data *dt, int x, int y, int color);
void				point(t_data *dt, int x, int y, int color);
int					color(int i, double z, double c);
/*
**	MATH.C
**	-----------------------------------------------------------------
*/
t_point				map(double x, double y, t_screen *s, t_map *m);
double				interpolate(double start, double end, double interpolation);
void				coef_update(t_data *dt);
/*
** HOOKS.C
**	-----------------------------------------------------------------
*/
int					zoom(int button, int x, int y, t_data *dt);
int					controls(int button, t_data *dt);
int					mouse_check(int x, int y, t_data *dt);
/*
**	INDEX
**	----------------------------------------------------------------
*/
void				index_check(double mousex, double mousey, t_data *dt);

void				index_zero(t_data *dt);
int					calc_zero(t_point p, int i);

void				index_one(t_data *dt);
int					calc_one(t_point p, int i);

void				index_two(double mousex, double mousey, t_data *dt);
int					calc_two(double mousex, double mousey, t_point p, \
					int max_i);

void				index_three(double mousex, double mousey, t_data *dt);
int					calc_three(double mousex, double mousey, t_point p, \
					int max_i);
/*
**	INDEX_HELPER.C
**	----------------------------------------------------------------
*/
void				parallel_init(t_data *dt);
void				*index_zero_ploop(void *arg);
void				*index_one_ploop(void *arg);
void				*index_two_ploop(void *arg);
void				*index_three_ploop(void *arg);

void				screen_init(t_pixel *dot);
void				limits_set(t_point *p0, t_point *p1, t_data *dt);
void				calc_zero_init(t_point *p, t_nums *n, int *i);
void				calc_two_init(t_point *p, t_nums *n, double mx, double my);

#endif
