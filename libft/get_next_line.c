/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/10 16:53:36 by avykhova          #+#    #+#             */
/*   Updated: 2018/02/10 16:53:40 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

t_list		*right_file(t_list **head, int fd)
{
	t_list	*tmp;

	tmp = *head;
	if (fd < 0)
		return (NULL);
	while (tmp)
	{
		if (tmp->content_size == (size_t)fd)
			return (tmp);
		if (tmp->next)
			tmp = tmp->next;
		else
			break ;
	}
	if (!(tmp = ft_lstnew(NULL, 0)))
		return (NULL);
	ft_lstadd(head, tmp);
	tmp->content_size = fd;
	return (tmp);
}

int			read_buffer(t_list *current)
{
	char	*b;
	char	*t;
	int		r;

	E((b = ft_strnew(BUFF_SIZE)));
	if ((r = read(current->content_size, b, BUFF_SIZE)) <= 0)
	{
		free(b);
		return (r);
	}
	if (current->content)
	{
		E((t = ft_strjoin(current->content, b)));
		free(current->content);
		free(b);
		current->content = t;
	}
	else
		current->content = b;
	return (r);
}

int			extract_line(t_list *current, char **line, int i)
{
	char	*tmp;
	int		len;

	len = ft_strlen(current->content);
	E((*line = ft_strsub(current->content, 0, i)));
	if (len - i > 1)
	{
		E((tmp = ft_strsub(current->content, i + 1, len - (i - 1))));
		free(current->content);
		current->content = tmp;
	}
	else
	{
		free(current->content);
		current->content = NULL;
	}
	return (1);
}

int			last_line(t_list *current, char **line)
{
	if (current->content && ft_strlen(current->content) > 0)
	{
		if (!(*line = ft_strdup(current->content)))
			return (-1);
		else
		{
			free(current->content);
			current->content = NULL;
			return (1);
		}
	}
	return (0);
}

int			get_next_line(const int fd, char **line)
{
	static t_list	*fd_lst;
	t_list			*current;
	int				r;
	int				i;

	E((current = right_file(&fd_lst, fd)));
	if (read(fd, current->content, 0) == -1)
		return (-1);
	if (!current->content && ((r = read_buffer(current)) <= 0))
	{
		if (current->next)
			fd_lst = current->next;
		free(current);
		return (r == 0) ? 0 : -1;
	}
	i = 0;
	while (current->content[i])
	{
		if (current->content[i] == '\n')
			return (extract_line(current, line, i));
		if (i == (int)(ft_strlen(current->content) - 1))
			r = read_buffer(current);
		++i;
	}
	return (last_line(current, line));
}
