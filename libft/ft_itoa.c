/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/04 13:21:47 by avykhova          #+#    #+#             */
/*   Updated: 2018/01/04 13:21:48 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static long	long_pow(long base, int exponent)
{
	if (exponent == 0)
		return (1);
	else
		return (base * long_pow(base, exponent - 1));
}

static int	num_size(long n)
{
	int		i;
	int		sign;

	sign = 0;
	i = 0;
	if (n < 0)
	{
		n = n * (-1);
		sign = 1;
	}
	else if (n == 0)
		return (1);
	while (n - long_pow(10, i) > 0)
		i++;
	if (n - long_pow(10, i) == 0)
		i++;
	return (i + sign);
}

static char	*mechanics(int size, long nn, char *numb, int sign)
{
	while (size >= 0)
	{
		numb[size] = (nn % 10) + 48;
		nn = nn / 10;
		if (sign && size == 0)
			numb[size] = '-';
		size--;
	}
	return (numb);
}

char		*ft_itoa(int n)
{
	long	nn;
	char	*numb;
	int		size;
	int		sign;

	nn = n;
	sign = 0;
	size = num_size(nn);
	if (size <= 0)
		return (NULL);
	numb = ft_strnew(size);
	if (!numb)
		return (NULL);
	if (nn < 0)
	{
		sign = 1;
		nn = nn * (-1);
	}
	size--;
	return (mechanics(size, nn, numb, sign));
}
