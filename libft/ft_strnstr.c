/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/18 13:14:37 by avykhova          #+#    #+#             */
/*   Updated: 2017/12/22 19:01:11 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *s, const char *sub, size_t len)
{
	size_t	i;
	size_t	si;
	size_t	sublen;

	i = 0;
	si = 0;
	sublen = ft_strlen(sub);
	if (!*sub)
		return ((char *)s);
	if (s == sub)
		return ((char *)s);
	while (s[i] != '\0')
	{
		while (s[i + si] == sub[si] && (i + si) < len && sub[si] && s[i + si])
			si++;
		if (si == sublen)
			return (&((char *)s)[i]);
		si = 0;
		i++;
	}
	return (NULL);
}
