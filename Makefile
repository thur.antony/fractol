# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: avykhova <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/26 14:41:25 by avykhova          #+#    #+#              #
#    Updated: 2018/03/26 14:41:27 by avykhova         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fractol
LIBFT_NAME = ./libft/libft.a

SRC_PROJECT = ./src/main.c \
			./src/main_helper.c \
			./src/draw.c \
			./src/math.c \
			./src/hooks.c \
			./src/index_helper.c \
			./src/index_check.c \
			./src/index_zero.c \
			./src/index_one.c \
			./src/index_two.c \
			./src/index_three.c \
			./src/parallel_init.c

OBJECT_PROJECT = $(SRC_PROJECT:.c=.o)
FLAGS = -O3 -Wall -Wextra -Werror -Iincludes/
FLAGS_MLX = -lmlx -framework OpenGL -framework AppKit
FLAGS_MLX_ELEMENTARY = - lpthread -lmlx -lXext -lX11 -lm -L ./libmlx/ 
FLAGS_LIBFT = -L./libft -lft

all: $(NAME)
	@echo '🥑 compiling a project for iMac'
	@gcc -o $(NAME) $(FLAGS) $(SRC_PROJECT) $(FLAGS_LIBFT) $(FLAGS_MLX)

lin: $(NAME)
	@echo '🥑 compiling a project for linux'
	@gcc -o $(NAME) $(FLAGS) $(SRC_PROJECT) $(FLAGS_LIBFT) $(FLAGS_MLX_ELEMENTARY)

libft:
	@echo '🥑 recompiling library libft'
	@make -C ./libft/ re

$(NAME): $(LIBFT_NAME) $(OBJECT_PROJECT)
	
$(LIBFT_NAME):
	@echo '🥑 recompiling an cleaning library libft'
	@make -C ./libft/ re
	@make -C ./libft/ clean	

%.o: %.c
	@echo '🥑 updating changes in .c files'
	@gcc $(FLAGS) -o $@ -c $<
clean:
	@echo '🥑 cleaning a project'
	@/bin/rm -f $(OBJECT_PROJECT)
fclean: clean
	@echo '🥑 removing a project'
	@/bin/rm -f $(NAME)
	@make -C ./libft/ fclean
re:	fclean all

l: lin clean
